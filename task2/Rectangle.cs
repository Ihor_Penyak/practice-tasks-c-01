using System;

namespace ShlemkevychSerhiy.PracticeTasks.Task2
{
	// IP: ��� ���������������
	// error CS0841: Cannot use local variable 'b' before it is declared
	class Rectangle
	{
		private double _x;
		private double _y;
		private double _width;
		private double _heght;
		
		public Rectangle(double leftBottomX, double leftBottomY, double width, double height)
		{
			_x  = leftBottomX;
			_y  = leftBottomY;
			_width = width;
			_heght = height;
		}
		
		public double X
		{
			get
			{
				return _x;
			}
		}
		public double Y
		{
			get
			{
				return _y;
			}
			
		}
		public double Width
		{
			get
			{
				return _width;
			}
		}
		public double Height
		{
			get
			{
				return _heght;
			}
		}
		
		public void Move(double x, double y)
		{
			_x = x;
			_y = y;
		}
		
		public void Resize(double width, double height)
		{
			_width = width;
			_heght = height;
		}
		
		public override string ToString()
		{
			return String.Format("Rectangle, coordinates of bottom left point are: x = {0}, y = {1}; width = {2}, height = {3}", X, Y, Width, Height);
		}
		
		public static Rectangle Intersect(Rectangle left, Rectangle right)
		{
			double x = Math.Max(left.X, right.X);
			double y = Math.Max(left.Y, right.Y);
			double width = Math.Min(left.X + left.Width, right.X + right.Width) - x;
			double height = Math.Min(left.Y + left.Height, right.Y + right.Height) - y;

            return new Rectangle(x, y, width, height);
		}
		
		public static Rectangle Union(Rectangle left, Rectangle right)
		{
			double x = Math.Min(left.X, right.X);
			double y = Math.Min(left.Y, right.Y);
			double width = Math.Max(left.X + left.Width, right.X + right.Width) - x;
			double height = Math.Max(left.Y + left.Height, right.Y + right.Height) - y;
			
			return new Rectangle(x, y, width, height);
		}
	}
	
	class Program
	{
		static void Main()
		{
			Rectangle a = new Rectangle(0, 0, 2, 10);
			
			Console.WriteLine(a.ToString());			
			Console.WriteLine("Changing width by 20 and moving to [5, 5]");
			
			a.Resize(22, a.Height);
			a.Move(5, 5);
			
			Console.WriteLine("Here is the result: {0}", b.ToString());
			
			Rectangle b = new Rectangle(0, 0, 6, 7);
			
			Console.WriteLine("Let`s create one more rectangle b: {0}", a.ToString());

			Console.WriteLine("Union of these two is: {0}", Rectangle.Union(a, b).ToString());
			Console.WriteLine("Intersection of these two is: {0}", Rectangle.Intersect(a, b).ToString());
			
			Console.ReadKey();
		}
	}
}