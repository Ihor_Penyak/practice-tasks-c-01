using System;

namespace ShlemkevychSerhiy.PracticeTasks.Task1
{
    //
	// �P: ��� ���������  ��������� ����� warnings, ���'����� � ���������� �-�� GetHashCode
	//
	
	class Vector : IComparable
    {
        private double _x;
        private double _y;
        private double _z;

        public Vector(double x, double y, double z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        public double X
        {
            get
            {
                return _x;
            }
        }

        public double Y
        {
            get
            {
                return _y;
            }
        }

        public double Z
        {
            get
            {
                return _z;
            }
        }

        public void Add(Vector vector)
        {
            _x += vector.X;
            _y += vector.Y;
            _z += vector.Z;
        }

        public void Substract(Vector vector)
        {
            _x -= vector.X;
            _y -= vector.Y;
            _z -= vector.Z;
        }

        public double GetLength()
        {
            return Math.Sqrt(Math.Pow(X, 2.0) + Math.Pow(Y, 2.0) + Math.Pow(Z, 2.0));
        }

        public int CompareTo(object obj)
        {
            return GetLength().CompareTo((obj as Vector).GetLength());
        }

        public override bool Equals(object obj)
        {
            // if (obj == null)
            // {
                // return false;
            // }

            Vector vector = obj as Vector;

            // if (vector == null)
            // {
                // return false;
            // }

			//return (vector.X == X && vector.Y == Y && vector.Z == Z);
			
			// IP: ��� ����� ����������� ...
			return (vector != null) && (vector.X == X && vector.Y == Y && vector.Z == Z)
        }

        public override string ToString()
        {
            return String.Format("Vector, x = {0}, y = {1}, z = {2}", X, Y, Z);
        }

        public static Vector Add(Vector left, Vector right)
        {
            double x = left.X + right.X;
            double y = left.Y + right.Y;
            double z = left.Z + right.Z;

            return new Vector(x, y, z);
        }

        public static Vector Substract(Vector left, Vector right)
        {
            double x = left.X - right.X;
            double y = left.Y - right.Y;
            double z = left.Z - right.Z;

            return new Vector(x, y, z);
        }

        public static Vector VectorMultiply(Vector left, Vector right)
        {
            double x = (left.Y * right.Z) - (left.Z * right.Y);
            double y = (left.Z * right.X) - (left.X * right.Z);
            double z = (left.X * right.Y) - (left.Y * right.X);

            return new Vector(x, y, z);
        }

        public static double ScalarMultiply(Vector left, Vector right)
        {
            return (left.X * right.X) + (left.Y * right.Y) + (left.Z * right.Z);
        }

        public static double MixedMultiply(Vector scalar, Vector left, Vector right)
        {
            return ScalarMultiply(scalar, VectorMultiply(left, right));
        }

        public static double GetAngle(Vector left, Vector right)
        {
            return Math.Acos((Vector.ScalarMultiply(left, right)) / (left.GetLength() * right.GetLength()));
        }

        public static Vector operator +(Vector left, Vector right)
        {
            return Add(left, right);
        }

        public static Vector operator -(Vector left, Vector right)
        {
            return Substract(left, right);
        }

        public static bool operator ==(Vector left, Vector right)
        {
            // IP: ������� �� ��������� �������� �� ��������� �-� �-�� "return" � �-��� ...
			if (System.Object.ReferenceEquals(left, right))
            {
                return true;
            }

            if ((object)left == null || (object)right == null)
            {
                return false;
            }

            return Equals(left, right);
        }

        public static bool operator !=(Vector left, Vector right)
        {
            if (System.Object.ReferenceEquals(left, right))
            {
                return false;
            }

            if ((object)left == null || (object)right == null)
            {
                return true;
            }

            return !Equals(left, right);
        }

        // IP: �� ���������, �� ������� �� ����������� ��'���� ����� ���������� �� �����, �������� ���� �� �� ������, ��� �� ����� �� ���������� ������ ... ...
		public static bool operator >(Vector left, Vector right)
        {
            return left.CompareTo(right) > 0;
        }

        public static bool operator <(Vector left, Vector right)
        {
            return left.CompareTo(right) < 0;
        }

        public static bool operator >=(Vector left, Vector right)
        {
            return left.CompareTo(right) >= 0;
        }

        public static bool operator <=(Vector left, Vector right)
        {
            return left.CompareTo(right) <= 0;
        }
    }

    class Program
    {
        static void Main()
        {
            Vector a = new Vector(1, 2, 3);
            Console.WriteLine("Let`s create vector a: {0}", a.ToString());

            Console.WriteLine("Length of this vector equals {0:0.000}", a.GetLength());

            Vector b = new Vector(8, 2, 4);
            Console.WriteLine("Let`s create one more vector - b: {0}", b.ToString());

            Console.WriteLine("Sum of these vectors equals {0}", Vector.Add(a, b).ToString());

            Console.WriteLine("Difference of these vectors equals {0}", Vector.Substract(a, b).ToString());

            Console.WriteLine("Scalar product of these vectors equals {0}", Vector.ScalarMultiply(a, b).ToString());
            Console.WriteLine("Vector product of these vectors equals {0}", Vector.VectorMultiply(a, b).ToString());
            Console.WriteLine("Mixed product of these vectors (a*(a*b)) equals {0}", Vector.MixedMultiply(a, a, b).ToString());

            if (a.Equals(b))
            {
                Console.WriteLine("a and b are equal");
            }
            else
            {
                Console.WriteLine("a is {0} then b", a > b ? "grater" : "less");
            }

            Console.WriteLine("Angle between these two vectors is {0:0.000} radian", Vector.GetAngle(a, b));

            Console.ReadKey();
        }
    }
}