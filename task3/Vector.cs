using System;
using System.Text;

namespace ShlemkevychSerhiy.PracticeTasks.Task3
{
    //
	// �P: ��� ���������  ��������� ����� warnings, ���'����� � ���������� �-�� GetHashCode
	//
	
	class Vector
    {
        private int[] _array;
        private int _startIndex;

        public Vector(int length)
        {
            _array = new int[length];
            _startIndex = 0;
        }

        public Vector(int startIndex, int length)
        {
            _array = new int[length];
            _startIndex = startIndex;
        }

        public int StartIndex
        {
            get
            {
                return _startIndex;
            }
        }
        public int Length
        {
            get
            {
                return _array.Length;
            }
        }

        public int this[int index]
        {
            get
            {
                if (index < StartIndex || index >= Length + StartIndex)
                {
                    throw new IndexOutOfRangeException();
                }

                return _array[index - StartIndex];
            }
            set
            {
                if (index < StartIndex || index >= Length + StartIndex)
                {
                    throw new IndexOutOfRangeException();
                }

                _array[index - StartIndex] = value;
            }
        }

        public void ScalarMultiply(int multipler)
        {
            for (var i = StartIndex; i < Length + StartIndex; i++)
            {
                this[i] *= multipler;
            }
        }

        public void Substract(Vector vector)
        {
			if (vector.StartIndex != StartIndex || vector.Length != Length)
            {
                throw new InvalidOperationException();
            }

            for (var i = StartIndex; i < Length + StartIndex; i++)
            {
                this[i] -= vector[i];
            }
        }

        public void Add(Vector vector)
        {
            if (vector.StartIndex != StartIndex || vector.Length != Length)
            {
                throw new InvalidOperationException();
            }

            for (var i = StartIndex; i < Length + StartIndex; i++)
            {
                this[i] += vector[i];
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Vector - {");

            for (var i = StartIndex; i < Length + StartIndex; i++)
            {
                builder.Append(String.Format(" {0},", this[i]));
            }

            builder.Append("}");

            return builder.ToString();
        }

        public override bool Equals(object obj)
        {
            // IP: ������������ ��� - ����� ������ �-�� "return"
			if (obj == null)
            {
                return false;
            }

            Vector vector = obj as Vector;

            if (vector == null)
            {
                return false;
            }

            if (vector.StartIndex != StartIndex || vector.Length != Length)
            {
                return false;
            }

            for (var i = StartIndex; i < Length + StartIndex; i++)
            {
                if (this[i] != vector[i])
                {
                    return false;
                }
            }

            return true;
        }

        public static Vector ScalarMultiply(Vector vector, int multipler)
        {
            Vector result = new Vector(vector.StartIndex, vector.Length);

            for (var i = vector.StartIndex; i < vector.Length + vector.StartIndex; i++)
            {
                result[i] = vector[i] * multipler;
            }

            return result;
        }

        public static Vector Substract(Vector left, Vector right)
        {
            // IP: � �� �� ����� ���� ����������� ������� ��� ��� 
			// public void Substract(Vector vector) ???
			
			if (left.StartIndex != right.StartIndex || left.Length != right.Length)
            {
                throw new InvalidOperationException();
            }

            Vector result = new Vector(left.StartIndex, left.Length);

            for (var i = result.StartIndex; i < result.Length + result.StartIndex; i++)
            {
                result[i] = left[i] - right[i];
            }

            return result;
        }

        public static Vector Add(Vector left, Vector right)
        {
            if (left.StartIndex != right.StartIndex || left.Length != right.Length)
            {
                throw new InvalidOperationException();
            }

            Vector result = new Vector(left.StartIndex, left.Length);

            for (var i = result.StartIndex; i < result.Length + result.StartIndex; i++)
            {
                result[i] = left[i] + right[i];
            }

            return result;
        }

        public static Vector operator +(Vector left, Vector right)
        {
            return Add(left, right);
        }

        public static Vector operator -(Vector left, Vector right)
        {
            return Substract(left, right);
        }

        public static Vector operator *(Vector vector, int multipler)
        {
            return ScalarMultiply(vector, multipler);
        }

        public static Vector operator *(int multipler, Vector vector)
        {
            return ScalarMultiply(vector, multipler);
        }

        public static bool operator ==(Vector left, Vector right)
        {
            if (System.Object.ReferenceEquals(left, right))
            {
                return true;
            }

            if ((object)left == null || (object)right == null)
            {
                return false;
            }

            return Equals(left, right);
        }

        public static bool operator !=(Vector left, Vector right)
        {
            if (System.Object.ReferenceEquals(left, right))
            {
                return false;
            }

            if ((object)left == null || (object)right == null)
            {
                return true;
            }

            return !Equals(left, right);
        }
    }

    class Program
    {
        static void Main()
        {
            Vector a = new Vector(5, 2);
            a[5] = 1;
            a[6] = 3;
            Vector b = new Vector(5, 2);
            b[5] = 1;
            b[6] = 2;

            Console.WriteLine("Let`s create a pair of vectors: \n a - {0},\n b - {1}", a.ToString(), b.ToString());

            Console.WriteLine("a + b = {0}", Vector.Add(a, b).ToString());

            Console.WriteLine("a*2 - b = {0}", Vector.Substract(Vector.ScalarMultiply(a, 2), b).ToString());

            Console.WriteLine("Are a and b equal? - {0}", a.Equals(b) ? "yes" : "no");

            Console.ReadKey();
        }
    }
}